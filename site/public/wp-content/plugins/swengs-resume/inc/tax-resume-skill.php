<?php

namespace Swengs\Resume;

// Register Custom Taxonomy
add_action( 'init', function () {
	$labels = [
		'name'                       => _x( 'Skills', 'Taxonomy General Name', 'swengs-resume' ),
		'singular_name'              => _x( 'Skill', 'Taxonomy Singular Name', 'swengs-resume' ),
		'menu_name'                  => __( 'Skills', 'swengs-resume' ),
		'all_items'                  => __( 'All Skills', 'swengs-resume' ),
		'parent_item'                => __( 'Parent Skill', 'swengs-resume' ),
		'parent_item_colon'          => __( 'Parent Skill:', 'swengs-resume' ),
		'new_item_name'              => __( 'New Skill', 'swengs-resume' ),
		'add_new_item'               => __( 'Add New Skill', 'swengs-resume' ),
		'edit_item'                  => __( 'Edit Skill', 'swengs-resume' ),
		'update_item'                => __( 'Update Skill', 'swengs-resume' ),
		'view_item'                  => __( 'View Skill', 'swengs-resume' ),
		'separate_items_with_commas' => __( 'Separate skills with commas', 'swengs-resume' ),
		'add_or_remove_items'        => __( 'Add or remove skills', 'swengs-resume' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'swengs-resume' ),
		'popular_items'              => __( 'Popular Skills', 'swengs-resume' ),
		'search_items'               => __( 'Search Skills', 'swengs-resume' ),
		'not_found'                  => __( 'Not Found', 'swengs-resume' ),
		'no_terms'                   => __( 'No skills', 'swengs-resume' ),
		'items_list'                 => __( 'Skills list', 'swengs-resume' ),
		'items_list_navigation'      => __( 'Skills list navigation', 'swengs-resume' ),
	];

	$args = [
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	];

	register_taxonomy( 'swengs_resume_skill', [ 'post', 'swengs_resume_item' ], $args );
});
