<?php

namespace Swengs\Resume;

/**
 * Register custom post type for a resume item.
 */
function register_resume_item_cpt() {
	register_post_type( 'swengs_resume_item', [
		'labels'            => [
			'name'                => __( 'Resume Items', 'swengs-resume' ),
			'singular_name'       => __( 'Resume Item', 'swengs-resume' ),
			'all_items'           => __( 'All Resume Items', 'swengs-resume' ),
			'new_item'            => __( 'New Resume Item', 'swengs-resume' ),
			'add_new'             => __( 'Add New', 'swengs-resume' ),
			'add_new_item'        => __( 'Add New Resume Item', 'swengs-resume' ),
			'edit_item'           => __( 'Edit Resume Item', 'swengs-resume' ),
			'view_item'           => __( 'View Resume Item', 'swengs-resume' ),
			'search_items'        => __( 'Search Resume Items', 'swengs-resume' ),
			'not_found'           => __( 'No Resume Items found', 'swengs-resume' ),
			'not_found_in_trash'  => __( 'No Resume Items found in trash', 'swengs-resume' ),
			'parent_item_colon'   => __( 'Parent Resume Item', 'swengs-resume' ),
			'menu_name'           => __( 'Resume Items', 'swengs-resume' ),
		],
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => [ 'title', 'editor' ],
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'resumeitem',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	] );
}
add_action( 'init', 'Swengs\Resume\register_resume_item_cpt');

/**
 * Register messages.
 */
add_filter( 'post_updated_messages', function ( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['swengs_resume_item'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Resume Item updated. <a target="_blank" href="%s">View Resume Item</a>', 'swengs-resume'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swengs-resume'),
		3 => __('Custom field deleted.', 'swengs-resume'),
		4 => __('Resume Item updated.', 'swengs-resume'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Resume Item restored to revision from %s', 'swengs-resume'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Resume Item published. <a href="%s">View Resume Item</a>', 'swengs-resume'), esc_url( $permalink ) ),
		7 => __('Resume Item saved.', 'swengs-resume'),
		8 => sprintf( __('Resume Item submitted. <a target="_blank" href="%s">Preview Resume Item</a>', 'swengs-resume'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Resume Item scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Resume Item</a>', 'swengs-resume'),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Resume Item draft updated. <a target="_blank" href="%s">Preview Resume Item</a>', 'swengs-resume'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
});

/**
 * Register ACF fields.
 */
add_action( 'acf/init', function () {
	acf_add_local_field_group( [
		'key' => 'group_swengs_resume_item',
		'title' => __('Resume Item', 'swengs-resume'),
		'position' => 'acf_after_title',
		'style' => 'seamless',
		'location' => [
			[
				[
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'swengs_resume_item',
				],
			],
		],
		'fields' => [
			[
				'key' => 'field_swengs_resume_item_position',
				'label' => __('Position', 'swengs-resume'),
				'name' => 'swengs_resume_item_position',
				'type' => 'text',
			],
			[
				'key' => 'field_swengs_resume_item_start',
				'label' => __('Start date', 'swengs-resume'),
				'name' => 'swengs_resume_item_start',
				'type' => 'date_picker',
			],
			[
				'key' => 'field_swengs_resume_item_end',
				'label' => __('End date', 'swengs-resume'),
				'name' => 'swengs_resume_item_end',
				'type' => 'date_picker',
			],
			[
				'key' => 'field_swengs_resume_item_organization',
				'label' => __('Organization', 'swengs-resume'),
				'name' => 'swengs_resume_item_organization',
				'type' => 'taxonomy',
				'taxonomy' => 'swengs_resume_organization',
				'field_type' => 'select',
				'allow_null' => 0,
				'load_save_terms' => 1,
				'return_format'	=> 'object',
				'add_term' => 1,
			],
		],
	] );
});

/**
 * Hide title field in editor.
 */
add_action( 'admin_init', function () {
	remove_post_type_support('swengs_resume_item', 'title');
});

/**
 * Generate title from other fields.
 */
add_filter('title_save_pre', function ( $title ) {
	global $post;
	if ( isset( $post->ID ) && $post->post_type === 'swengs_resume_item' ) {
		$position = $_POST['acf']['field_swengs_resume_item_position'] ??
		            get_field( 'swengs_resume_item_position', $post->ID);
		$start = $_POST['acf']['field_swengs_resume_item_start'] ??
		         get_field( 'swengs_resume_item_position', $post->ID);
		$end = $_POST['acf']['field_swengs_resume_item_end'] ??
		       get_field( 'swengs_resume_item_position', $post->ID);

		$title = sprintf(
			'%s, %s - %s',
			$position,
			$start,
			$end
		);
	}

	return $title;
});
