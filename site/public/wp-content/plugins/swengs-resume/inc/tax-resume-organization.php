<?php

namespace Swengs\Resume;

// Register Custom Taxonomy
add_action( 'init', function () {
	$labels = array(
		'name'                       => _x( 'Organizations', 'Taxonomy General Name', 'swengs-resume' ),
		'singular_name'              => _x( 'Organization', 'Taxonomy Singular Name', 'swengs-resume' ),
		'menu_name'                  => __( 'Organizations', 'swengs-resume' ),
		'all_items'                  => __( 'All Organizations', 'swengs-resume' ),
		'parent_item'                => __( 'Parent Organization', 'swengs-resume' ),
		'parent_item_colon'          => __( 'Parent Organization:', 'swengs-resume' ),
		'new_item_name'              => __( 'New Organization', 'swengs-resume' ),
		'add_new_item'               => __( 'Add New Organization', 'swengs-resume' ),
		'edit_item'                  => __( 'Edit Organization', 'swengs-resume' ),
		'update_item'                => __( 'Update Organization', 'swengs-resume' ),
		'view_item'                  => __( 'View Organization', 'swengs-resume' ),
		'separate_items_with_commas' => __( 'Separate organizations with commas', 'swengs-resume' ),
		'add_or_remove_items'        => __( 'Add or remove organizations', 'swengs-resume' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'swengs-resume' ),
		'popular_items'              => __( 'Popular Organizations', 'swengs-resume' ),
		'search_items'               => __( 'Search Organizations', 'swengs-resume' ),
		'not_found'                  => __( 'Not Found', 'swengs-resume' ),
		'no_terms'                   => __( 'No organizations', 'swengs-resume' ),
		'items_list'                 => __( 'Organizations list', 'swengs-resume' ),
		'items_list_navigation'      => __( 'Organizations list navigation', 'swengs-resume' ),
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'swengs_resume_organization', array( 'swengs_resume_item' ), $args );
});
