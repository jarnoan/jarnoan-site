<?php
/*
Plugin Name:  Swengs Resume
Plugin URI:   https://jarnoan.com/swengs/resume
Description:  Plugin for managing resume entries
Version:      0.1.1
Author:       Jarno Antikainen
Author URI:   https://jarnoan.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  swengs-resume
Domain Path:  /languages

Swengs Resume is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Swengs Resume is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Swengs Resume. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

namespace Swengs\Resume;

defined( 'ABSPATH' ) or die( 'Direct access not allowed' );

require 'inc/cpt-resume-item.php';
require 'inc/tax-resume-organization.php';
require 'inc/tax-resume-skill.php';

register_activation_hook( __FILE__, function () {
	register_resume_item_cpt();
	flush_rewrite_rules();
});

register_deactivation_hook( __FILE__, function() {
	flush_rewrite_rules();
});
